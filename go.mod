module 0xacab.org/leap/vpn-hole/vpnhole

go 1.17

require (
	0xacab.org/leap/vpn-hole v0.0.0-20221021064936-cd8e60763fd9
	github.com/common-nighthawk/go-figure v0.0.0-20210622060536-734e95fb86be
	github.com/miekg/dns v1.1.50
)

require (
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/net v0.0.0-20220722155237-a158d28d115b // indirect
	golang.org/x/sync v0.0.0-20220819030929-7fc1605a5dde // indirect
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f // indirect
	golang.org/x/tools v0.1.12 // indirect
)
